from flask import request
import telegram
#from stakit import StakitBot
from stakit.database.Database import manager, app
from stakit.StakitBot import webhook
from stakit.setup import bbot

#============setup=============================

bot = bbot()
#===============================================


@app.route('/ss', methods=['POST', 'GET'])
def botIndex():
    if request.method == 'POST':
        update = telegram.Update.de_json(request.get_json(force=True), bot)
        webhook(update)
        return 'ok', 200

@app.route('/end/<query>')
def endBot(query):
    if query == 'end':
        pass
        #StakitBot.stopBot()
    return 'ok', 200

@app.route('/check')
def check():
    return 'ok', 200



if __name__ == '__main__':
    manager.run()
class Reply(object):
    """Bot reply to commands"""
    def __init__(self):
        # Inittiates Bot replies to different commands

        self._start = '''
Hello <b>{}</b>
Thanks for using $takit.
Kindly go through our 
/terms_and_conditions'''
        self._terms ='''
Read our <b>terms and conditions</b>.
<a href='google.com'>here.</a>

<b>After going through our terms:</b>
Click /register to start your
stakit registration process. 
You can suspend or skip any
process by using command
/cancel and /skip respectively. 

<b>It is highly recommended
you complete the registration 
process before staking with us.</b>'''

        self._register='''
Do you agree to our terms
and conditions. Click
accept to continue. Click
reject to abort.'''

        self.term_accepted='''
Submit the referral code of
your referee. /skip if you 
are not referred.'''

        self.term_rejected='''
Operation cancelled because terms
was rejected.
'''

        self.have_referral='''
Referral set. 
Submit your btc withdrawal address
or /skip
'''
        self.skip_referral='''
Submit your btc withdrawal address
or /skip'''

        self.submit_address='''
Address succesfully saved.
Setup OTP
Copy and save your seed below.

<pre>
seed1
seed2
seed3
</pre>
Submit the generated code'''

        self.submit_invalid_address='''
Please submit a valid btc
address.'''

        self.otp_set='''
otp successfully set
Your profile
```
user_id:
deposit address(btc):
deposit address(goku):
withdrawal address(btc):
stake status:
```

'''


        self._help = '''
List of commands and uses'''

        self._help_command = '''
command {}
usage {}
example {}'''

        self._stake = '''
Stake initiated'''

        self._unstake = '''
Unstaking'''

        self._withdraw = '''
Withdrawal initiated'''

        self._balance = '''
Your balance is: {}'''

        self._profile = '''
Your profile details'''

        self._history = '''
Your history'''

        self._status = '''
Your current status'''

        self._total = '''
Total number of users is {}'''

#Error replies
        self._inputError ='''
Input Error'''
        self._unknownCommandError ='''
This is not a recognised command'''


    def start(self, username='New Staker'):
        # Reply to command start by greeting bot user
        return self._start.format(username)
    
    def terms(self):
        return self._terms

    def register(self):
        return self._register

    def help(self):
        pass

    def help_command(self):
        pass
    
    def stake(self):
        pass
    
    def unstake(self):
        pass

    def withdraw(self):
        pass
    
    def balance(self):
        pass

    def profile(self):
        pass
    
    def history(self):
        pass
    
    def status(self):
        pass

    def total(self):
        pass
    
    def unknownCommandError(self):
        return self._unknownCommandError



if __name__ == '__main__':

    test_reply_to = Reply() #initialise reply for unit test

    print('Testing start command: ', test_reply_to.start('Adeyemi'))

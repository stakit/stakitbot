"""
gen_btc_addr:   Generates bitcoin address for each user from
                electrum xpub.

gen_qrcode:     Generate Qrcode for addresses.

gen_otp:        Generate otp code.
"""
import bitcoin
import pyotp
import qrcode

def gen_btc_addr(master_pub, index):
    """ Generate btc address from electrum xpub"""
    return {index: bitcoin.pubtoaddr(bitcoin.bip32_descend(master_pub, [0, index]))}

def gen_otp_seed():
    """ Generate one time pass which is compactible with'
        google authenticator"""
    seed = pyotp.random_base32()
    return seed

def authy_uri(seed, user_id):
    """ Generate a uri readable by authenticator"""
    return pyotp.totp.TOTP(seed).provisioning_uri(user_id, issuer_name='stakit')

def is_valid_otp(otp, user):
    """confirm if the otp supplied by user is valid
        return True or False"""
    seed = user.otp_seed # add otp_seed to db
    code = pyotp.TOTP(seed)
    return code.verify(otp)

def addr_qrcode(address, coin, user):
    """ Generate Qrcode for addresses"""
    img = qrcode.make(address)
    img.save(user.user_id + coin + '.jpg') #save qrcode for each user/ for each coin
    return img


if __name__ == '__main__':
    masterpub = "xpub6DWtD5PUAvBKcguJsEbRx77TokpmTYvm3KmeLnbco1V4PG3FoP7bLW3FGAj7DFGLsqPK8ZUR37FxJHFAgKUDwtB5MmY5PSVPXETHBEDQn1H"
    print(gen_btc_addr(masterpub, 2))

    class User(object):
        """dummy user object"""
        def __init__(self, user_id, otp_seed):
            self.uid = user_id
            self.otp_seed = otp_seed

    class Dummyusers(object):
        """ Dummy class"""
        def __init__(self):
            """ instantiate a dummy class"""
            self.users = []
        def newuser(self, user_id):
            """ new user for the class"""
            if not get_user(user_id): #avoid same uid
                return self.users.append(User(user_id, otp_seed=gen_otp_seed()))

    dummy = Dummyusers()

    def get_user(user_id): #######for demo
        """ get a user from the dummy class if exist"""
        try:
            return (list(user for user in dummy.users if user.uid == user_id))[0]
        except IndexError:
            return False

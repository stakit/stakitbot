#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging 
#from telegram.ext import Updater
from telegram.ext import InlineQueryHandler
from telegram import (InlineQueryResultArticle, InputTextMessageContent)
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode
#from telegram import Bot
from telegram.ext import JobQueue

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram import (InlineKeyboardButton, InlineKeyboardMarkup)

from telegram.ext import (ConversationHandler, RegexHandler)
from telegram.ext import CallbackQueryHandler

from .reply import Reply
from .setup import Setup, bbot
#from database import
# from dbfunction import

bot = bbot()
#updater = Updater('419074433:AAE4xidBhcQGoHq1VrbI_gW0pV-S3AgA4ZM')
#dispatcher = updater.dispatcher
update_queue, dispatcher = Setup(bbot)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
reply_to = Reply()
'''u = Updater('419074433:AAE4xidBhcQGoHq1VrbI_gW0pV-S3AgA4ZM')
j = u.job_queue'''


job_queue = JobQueue(bot)
job_queue.start()

dispatcher.job_queue = job_queue


#def chatid():
#    return update.message.chat_id

#===========================FOR COMMAND======================================================================
def start(bot, update):
    username = update.message.from_user.username if update.message.from_user.username else update.message.from_user.first_name
    bot.send_message(chat_id=update.message.chat_id, text=reply_to.start(username), parse_mode=ParseMode.HTML)
    return
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

def terms(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=reply_to.terms(), parse_mode=ParseMode.HTML)    
terms_handler = CommandHandler('terms_and_conditions', terms)
dispatcher.add_handler(terms_handler)

def kill(bot, update):
    update.message.reply_text('killing')
    dispatcher.stop()
    update.message.reply_text('killed')
kill_handler = CommandHandler('kill', kill)
dispatcher.add_handler(kill_handler)
#===========================FOR INLINE======================================================================
def inline_eg(bot, update):
    ''' Enable bot to be called inline from bot father first
        then read docs telegram.ext.InlineQueryHandler, answerInlineQuery
        read more...'''
    query = update.inline_query.query
    if not query:
        return
    results = list()
    results.append(
        InlineQueryResultArticle(
            id=query.upper(),
            title='Caps',
            input_message_content=InputTextMessageContent(query.upper()) 
            )
            )
    bot.answer_inline_query(update.inline_query.id, results) #this defines the reply
inline_caps_handler = InlineQueryHandler(inline_eg)
dispatcher.add_handler(inline_caps_handler)

#=========================FOR CONVERSATION HANDLER -- REGISTRATION==========================================================
REG_REPLY, SET_REF, SUBMIT_ADDR, OTP_SETUP = range(4)
'''
def register(bot, update):
    register_keyboard = [['Accept', 'Reject']]
    update.message.reply_text(reply_to.register(), reply_markup=ReplyKeyboardMarkup(register_keyboard, one_time_keyboard=True))

    return REG_REPLY'''


def register(bot, update):
    register_button = [[
        InlineKeyboardButton('Accept', callback_data='Accept'),
        InlineKeyboardButton('Reject', callback_data='Reject')]]
    register_keyboard= InlineKeyboardMarkup(register_button) #, {'Accept':'Accept1', 'Reject':'Reject1'})
    update.message.reply_text(reply_to.register(), reply_markup=register_keyboard)

    return REG_REPLY

def cancel(bot, update):
    update.message.reply_text('Operation cancelled', reply_markup=ReplyKeyboardRemove()) #, reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END

def set_referral(bot, update):
    update.message.reply_text(reply_to.have_referral)
    return SUBMIT_ADDR

def skip_referral(bot, update):
    update.message.reply_text(reply_to.skip_referral)
    return SUBMIT_ADDR

def reg_status(bot, update): #edit_text used instead of send message
    query = update.callback_query
    user_reply = query.data
    chat_id = query.message.chat_id
    msg_id = query.message.message_id
    if user_reply == 'Reject':
        bot.edit_message_text(reply_to.term_rejected, chat_id=chat_id, message_id=msg_id) #, reply_markup=ReplyKeyboardRemove())
        return ConversationHandler.END
    else:
        #generate user_id
        #generate deposit wallet addre
        #setup account in database for user
        bot.edit_message_text(reply_to.term_accepted, chat_id=chat_id, message_id=msg_id)
        return SET_REF

def submit_addr(bot, update):
    address = update.message.text
    #check if address is a valid btc address
    #safe address
    update.message.reply_text(reply_to.submit_address + '\n your address: ' + address, parse_mode=ParseMode.HTML)
    return OTP_SETUP

def otp_setup(bot, update):
    otp_code = update.message.text
    #check otp code -- job queue
    #if success, send the below message
    #delete seed
    update.message.reply_text(reply_to.otp_set, parse_mode=ParseMode.MARKDOWN)
    return ConversationHandler.END


conv_handler = ConversationHandler(
    entry_points=[CommandHandler('register', register)],

    states={
        
        REG_REPLY: [CallbackQueryHandler(reg_status,)],
        #REG_REPLY: [RegexHandler('(Accept1|Reject1)', reg_status)]
        SET_REF: [MessageHandler(Filters.text, set_referral),
                    CommandHandler('skip', skip_referral)],
        SUBMIT_ADDR: [MessageHandler(Filters.text, submit_addr)],
        OTP_SETUP: [MessageHandler(Filters.text, otp_setup)]
        },
    fallbacks=[CommandHandler('cancel', cancel)]
)

dispatcher.add_handler(conv_handler)
#=========================FOR NORMAL MESSAGE SENT==========================================================
def echo(bot, update):
    '''This will handle normal message sent to the bot'''
    bot.send_message(chat_id=update.message.chat_id, text=update.message.text)
echo_handler = MessageHandler(Filters.text, echo)
dispatcher.add_handler(echo_handler)
#=========================FOR jobqueue==========================================================
def alarm(bot, job):
    bot.send_message(chat_id=job.context, text='BEEP')

def set_timer(bot, update, job_queue, chat_data):
    bot.send_message(chat_id=update.message.chat_id, text='setting timer')
    job = job_queue.run_once(alarm, 10, context=update.message.chat_id)
    chat_data['job'] = job
    update.message.reply_text('timer set')

def unset(update, chat_data):
    """Remove the job if the user changed their mind."""
    if 'job' not in chat_data:
        update.message.reply_text('You have no active timer')
        return

    job = chat_data['job']
    job.schedule_removal()
    del chat_data['job']

    update.message.reply_text('Timer successfully unset!')
dispatcher.add_handler(CommandHandler("set", set_timer,
                                  pass_args=False,
                                  pass_job_queue=True,
                                  pass_chat_data=True))
dispatcher.add_handler(CommandHandler("unset", unset, pass_chat_data=True))
#=========================Other commands after registration==========================================================
#balance, profile, stake, unstake, buy, sell, deposit, withdraw

def deposit(bot, update):
    pass

def withdraw(bot, update):
    pass

def stake(bot, update):
    pass

def unstake(bot, update):
    pass

def buy(bot, update):
    pass

def sell(bot, update):
    pass

def profile(bot, update):
    pass
    
def balance(bot, update):
    pass
#=========================FOR commands error==========================================================
#reply to unrecognised command
def unknownCommand(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=reply_to.unknownCommandError())
unknown_handler = MessageHandler(Filters.command, unknownCommand)   #define unrecognised message with MessageHandler, Filter.text
dispatcher.add_handler(unknown_handler, group=0) #group 0 has the highest priority



def webhook(update):
    update_queue.put(update)
'''
def startBot():
    updater.start_polling()
def stopBot():
    updater.stop()
def idleBot():
    #bot can be stopped with ctrl c
    updater.idle()
'''
def _webhook():
    ''' updater.start_webhook(listen='localhost',
                            port=5000,
                            url_path='sss')'''
    #updater.bot.set_webhook('https://c1289a82.ngrok.io/bot')
    pass

#updater.idle()

#initial setup of database with sqlite db settings.py
#from sys import argv
import os
from datetime import datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from dotenv import  load_dotenv, find_dotenv
from . import config



#setting up with sqlite
load_dotenv(find_dotenv())
basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
app.config.from_object(config.DevelopmentConfig)
app.config['SQLALCHEMY_MIGRATE_REPO'] = os.path.join(basedir, 'db_repository')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
#SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

class User(db.Model):
    __Tablename__ = 'User'

    _id = db.Column(db.Integer, primary_key=True)
    _uid = db.Column(db.String(30), unique=True) #hash or something unique
    _tid = db.Column(db.Integer, unique=True)
    _utype = db.Column(db.String(10)) #admin, staker-(mon,Tue,wed), dev, ceo 
    _reg_date = db.Column(db.DateTime)
    _status = db.Column(db.String(20)) #staking, not staking
    _not_done = db.Column(db.Integer) # steps needed to be completed [123]4567890 [important]
    _done = db.Column(db.Integer) #for steps completed
    _doing = db.Column(db.Integer)
    _refferedby = db.Column(db.String(30)) # uid is the refferal code
    _refferal_point = db.Column(db.Float) # increases as you invite more
    _total_reffered = db.Column(db.Integer)

    _wallet = db.relationship('Wallet', backref="user", lazy="dynamic")
    _history = db.relationship('History', backref="user", lazy="dynamic")
    _stake = db.relationship('Stake', backref="user", lazy="dynamic")

    def __init__(self, user_id, telegram_id, user_type, reg_date=datetime.now(), stake_status="not staking", not_done=1234567890,
                wallet_name='btc', wallet_for='deposit', wallet_address='1abcd345hshfhkjjjjjfdbbfdjfb',
                history_command='begin', comment='New account created'):
        self._uid = user_id
        self._tid = telegram_id
        self._utype = user_type
        self._reg_date = reg_date
        self._status = stake_status
        self._not_done = not_done
        self._done = 0
        self._doing = 0
        new_wallet = Wallet(wallet_name, wallet_for, wallet_address)
        new_history = History(history_command, comment, reg_date)
        self._wallet.append(new_wallet)
        self._history.append(new_history)
        db.session.add(new_wallet)
        db.session.add(new_history)
    

    #add methods (getter/setter) to check for utype, wallet name, uid(regex), 

class Wallet(db.Model):
    __Tablename__ = 'Wallet'

    _id = db.Column(db.Integer, primary_key=True) #address sequence
    _user_id = db.Column(db.String(30), db.ForeignKey('user._uid')) #uid of user who owns it
    _name = db.Column(db.String(10))#btc, eth, goku
    _for = db.Column(db.String(20))#deposit or withdrawal
    _address = db.Column(db.String(50)) # 1*** for btc, 0x*** for eth
    _balance = db.Column(db.Float) #current balance of address
    _to_be_returned_bal = db.Column(db.Float) #balance to be returned
    
    def __init__(self, wallet_name, wallet_for, wallet_address):
        self._name = wallet_name
        self._for = wallet_for
        self._address = wallet_address
        self._balance = 0
        self._to_be_returned_bal = 0 


class History(db.Model):
    __Tablename__ = 'History'

    _id = db.Column(db.Integer, primary_key=True) #index
    _for_user = db.Column(db.String(30), db.ForeignKey('user._uid')) #who created the history
    _command = db.Column(db.String(20))  #command that created the history
    _date = db.Column(db.DateTime) #date created
    _comment = db.Column(db.Text) #comment about this history; state value involved if any

    def __init__(self, command, comment, creation_date=datetime.now()):
        self._command = command
        self.date = creation_date
        self._comment = comment

class AdminTable(db.Model):
    __Tablename__ = 'Admin table'

    _id = db.Column(db.Integer, primary_key=True) #index
    _deposit_fee = db.Column(db.Float)
    _withdrawal_fee = db.Column(db.Float)
    _exchange_fee = db.Column(db.Float)
    _exchange_fee_discount = db.Column(db.Integer)
    _paused = db.Column(db.Integer) #true = 0, false = 1
    _deactivate_deposit = db.Column(db.Integer) #true = 0, false = 1
    _deactivate_withdrawal = db.Column(db.Integer)
    _deactivate_exchange = db.Column(db.Integer) #for buy and sell commands
    _deactivate_stake = db.Column(db.Integer)  #deactivate stake commands
    _total_users = db.Column(db.Integer)
    _total_stakers = db.Column(db.Integer)
    #to add, money in, money out etc, event date

class Stake(db.Model):
    _id = db.Column(db.Integer, primary_key=True)
    _owner = db.Column(db.String(30), db.ForeignKey('user._uid'))
    _start_date = db.Column(db.DateTime) #day staking begin
    _end_date = db.Column(db.DateTime) #day of unstake
    _coin = db.Column(db.String(30)) #goku, other staking coins
    _value = db.Column(db.Float)
    _day = db.Column(db.String(30)) #mon, tues, wed
    

def commit():
    return db.session.commit()

#add delete and add, add new functions to all



def main():
    pass

if __name__ == '__main__':
    manager.run() # file.py db init, migrate, upgrade ]   [file.py shell, runserver, ]
    
import os
from dotenv import find_dotenv, load_dotenv


#dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
#se = os.environ.get('DATABASE_URL')
#print(se)

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(find_dotenv())

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get("KEY")
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')


class ProductionConfig(Config):
    DEBUG = False



class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    TESTING = True
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')


class TestingConfig(Config):
    TESTING = True
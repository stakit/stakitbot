from queue import Queue
from threading import Thread

from telegram import Bot
from telegram.ext import Dispatcher


def bbot():
    return Bot('419074433:AAE4xidBhcQGoHq1VrbI_gW0pV-S3AgA4ZM')

def Setup(bott):
    #create bot, update queue and dispatcher instances
    bot = bott()
    bot.set_webhook('https://f796c09d.ngrok.io' + '/ss')
    update_queue = Queue()

    dispatcher = Dispatcher(bot, update_queue)
    #register handlers
    
    #start the thread
    thread = Thread(target=dispatcher.start, name='dispatcher')
    thread.start()

    return (update_queue, dispatcher) 


#dispatcher can be used to stop bot or register more handlers
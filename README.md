# Stakitbot #



### What is this repository for? ###

* A telegram bot
* To manage investors' staking account

### How do I get set up? ###

* Install python3, git and virtual environment
* Clone this repository
* Create a new virtual environment
* Branch off the develop branch
* pip install requirements.txt

### Contribution guidelines ###

* Test are necessary
* Fork the develop branch
* Make pull request against the develop branch

### Contact? ###

* @Skyyboat on telegram
* 